<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
 		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

    <title>Hello, world!</title>
  </head>
  <body>
    <h4 class="text-center mt-4 mb-4">Los datos a continuacion son obtenidos de Dolar-Monitor</h4>
    <div class="container">
    	<div class="row">
    		<div class="col-12">
    			<table id="example" class="table table-striped table-bordered" style="width:100%">
		        <thead>
		            <tr>
		                <th>Euro</th>
		                <th>Dolar</th>
		                <th>Dia</th>
		                <th>Fecha</th>
		            </tr>
		        </thead>
			    	<tbody>

		        
			    <?php
			$servername = "127.0.0.1";
			$database = "prueba2";
			$username = "root";
			$password = "";
			// Create connection
			$conn = mysqli_connect($servername, $username, $password, $database);
			// Check connection
			if (!$conn) {
			      die("Connection failed: " . mysqli_connect_error());
			}

			// CONEXION ESTABLECIDA CON LA BASE DE DATOS;

			// echo "Connected successfully";

			// CONSULTA AL API
			$content = file_get_contents("https://s3.amazonaws.com/dolartoday/data.json");
			$result  = json_decode($content);

			// DATOS A GUARDAR
			$euro = $result->EUR->dolartoday;
			$dolar = $result->USD->dolartoday;
			$dia = $result->_timestamp->dia;
			$fecha = $result->_timestamp->fecha_corta;

			// GUARDANDO DATOS EN LA BD
			$sql = "INSERT INTO history (euro, dolar, day, fecha) VALUES ('$euro', '$dolar', '$dia','$fecha')";
			if (mysqli_query($conn, $sql)) { ?>

				<div class="alert alert-success">
				  Actualizado con exito !¡
				</div>
				<?php
			} else {
			      echo "Error: " . $sql . "<br>" . mysqli_error($conn);
			}

			// $query = "SELECT * FROM history;";
			$sql = "SELECT * FROM history";
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
			    // output data of each row
			    while($row = $result->fetch_assoc()) {
			    	?>
		            <tr>
		                <td><?php echo $row["euro"] ?></td>
		                <td><?php echo $row["dolar"] ?></td>
		                <td><?php echo $row["day"] ?></td>
		                <td><?php echo $row["fecha"] ?></td>
		            </tr>
		        
			    <?php
			    }
			} else {
			    echo "0 results";
			}

			mysqli_close($conn);
		?>
						</tbody>
			    </table>
    		</div>
    	</div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
    	$(document).ready(function() {
			    $('#example').DataTable();
			} );
    </script>
  </body>
</html>



